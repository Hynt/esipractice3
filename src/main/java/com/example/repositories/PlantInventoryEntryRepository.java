package com.example.repositories;

import com.example.models.PlantInventoryEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by hynt on 2/26/16.
 */
@Repository
public interface PlantInventoryEntryRepository extends
        JpaRepository<PlantInventoryEntry, Long> {}
