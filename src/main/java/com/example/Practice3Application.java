package com.example;

import com.example.infrastructure.HibernateBasedIdentifierGenerator;
import com.example.models.PlantInventoryEntry;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Practice3Application {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(Practice3Application.class, args);

		//HibernateBasedIdentifierGenerator generator = context.getBean(HibernateBasedIdentifierGenerator.class);

		//System.out.println(generator.getID("PlantInventoryEntryIDGenerator"));
	}
}
