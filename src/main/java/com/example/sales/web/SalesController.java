package com.example.sales.web;

import com.example.sales.web.dto.QueryDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by hynt on 2/26/16.
 */

@Controller
@RequestMapping("/dashboard")
public class SalesController {
    @RequestMapping("catalog/form")
    public String getForm(Model model) {
        model.addAttribute("catalogQuery", new QueryDTO());
        return "dashboard/catalog/query-form";
    }

    @RequestMapping(method = RequestMethod.POST, value="/catalog/query")
    public String ExecuteQuery(Model model, QueryDTO query) {
        System.out.println("Potato__________________________");
        return "dashboard/catalog/query-result";
    }
}
