package com.example.models;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by hynt on 2/26/16.
 */


@Embeddable
@Value
@NoArgsConstructor(force=true)
@AllArgsConstructor(staticName="of")
public class PlantInventoryEntryID implements Serializable{
    Long id;
}
