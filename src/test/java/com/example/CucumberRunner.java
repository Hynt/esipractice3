package com.example;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by hynt on 2/26/16.
 */


@RunWith(Cucumber.class)
@CucumberOptions(plugin={"pretty","html:target/cucumber"})
public class CucumberRunner {
}


