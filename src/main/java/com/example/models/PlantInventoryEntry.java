package com.example.models;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by hynt on 2/26/16.
 */

@Entity
public class PlantInventoryEntry {
    @Id @EmbeddedId
    PlantInventoryEntryID id;

    String name;
    String description;

    @Column(precision=8,scale=2)
    BigDecimal price;
}
