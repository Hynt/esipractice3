package com.example.sales.web.dto;

import lombok.Data;

/**
 * Created by hynt on 2/26/16.
 */

@Data
public class QueryDTO {
    String name;
    BusinessPeriodDTO rentalPeriod;
}
