package com.example.models;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by hynt on 2/26/16.
 */

@Entity
public class PurchaseOrder {
    @Id
    @GeneratedValue
    Long id;

    //Relations
    //@OneToMany
    //List<PlantReservation> reservations;
    @OneToOne
    PlantInventoryEntry plant;

    LocalDate issueDate;
    LocalDate paymentSchedule;
    @Column(precision=8,scale=2)
    BigDecimal total;

    //@Enumerated(EnumType.STRING)
    //POStatus status;

    //@Embedded
    //BusinessPeriod rentalPeriod;
}
